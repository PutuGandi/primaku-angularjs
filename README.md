# Penggunaan API Node.js

##### 1. Cek list tag image di [Docker Hub](https://hub.docker.com/r/gandi12/angularjs/tags) yang akan digunakan, lalu pull image tersebut:
```bash
docker pull gandi12/angularjs:1
```

##### 2. Aplikasi di expose dengan port 80 dan kita bisa mapping ke port yang ingin kita hit, menjalankan image:
```bash
docker run -p <port-machine>:80 -d gandi12/angularjs:1
```

##### 3. akses app dengan url berikut:
```http
localhost:<port-machine>
```
