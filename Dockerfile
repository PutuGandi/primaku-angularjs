FROM node:alpine AS build-step
WORKDIR /app
COPY package*.json /app
RUN npm install 
COPY . /app
RUN npm run build --prod

FROM nginx:latest
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build-step /app/dist/hello-primaku .
ENTRYPOINT ["nginx","-g","daemon off;"]